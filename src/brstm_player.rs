#[allow(unused)]
use crate::brstm::BrstmFile;
use libmpv::{events::mpv_event_id::PlaybackRestart, FileState, Mpv};
use std::error::Error;

#[allow(dead_code)]
pub struct BrstmPlayer {
    brstm: BrstmFile,
    path: String,
    mpv: Mpv,
}

impl BrstmPlayer {
    pub fn setup(&self) -> Result<(), String> {
        let mpv = &self.mpv;
        let mut ready = false;
        let mut ev = mpv.create_event_context();
        let mut loop_str: Option<String> = None;
        if self.brstm.loop_flag() {
            loop_str = Some(format!(
                "ab-loop-a={},ab-loop-b={},ab-loop-count=inf",
                self.brstm.loop_start_time(),
                self.brstm.duration()
            ));
        }
        if let Err(e) = mpv.playlist_load_files(&[(
            self.path.as_str(),
            FileState::Replace,
            #[cfg(feature = "mpv-current")]
            None,
            loop_str.as_deref(),
        )]) {
            return Err(format!("ERROR: could not open \"{}\" ({e})", self.path));
        }

        // Block execution until the player is started
        let _ = ev.disable_all_events();
        let _ = ev.enable_event(PlaybackRestart);
        while !ready {
            if let Some(wrapped_event) = ev.wait_event(10.0) {
                let _ = wrapped_event.map_err(|e| format!("ERROR: could not unwrap event {e}"))?;
                ready = true;
            }
        }

        Ok(())
    }
    pub fn duration(&self) -> Result<f64, Box<dyn Error>> {
        Ok(self.mpv.get_property("duration")?)
    }
    pub fn time_pos(&self) -> Result<f64, Box<dyn Error>> {
        Ok(self.mpv.get_property("time-pos")?)
    }
    pub fn pause(&mut self) -> Result<(), Box<dyn Error>> {
        self.mpv.pause()?;
        Ok(())
    }
    pub fn percent_pos(&self) -> Result<i64, Box<dyn Error>> {
        Ok(self.mpv.get_property("percent-pos")?)
    }
    pub fn seek_absolute(&mut self, secs: f64) -> Result<(), Box<dyn Error>> {
        self.mpv.seek_absolute(secs)?;
        Ok(())
    }
    pub fn seek_percent_absolute(&mut self, percent: usize) -> Result<(), Box<dyn Error>> {
        self.mpv.seek_percent_absolute(percent)?;
        Ok(())
    }
    pub fn seek_percent_relative(&mut self, percent: isize) -> Result<(), Box<dyn Error>> {
        self.mpv.seek_percent(percent)?;
        Ok(())
    }
    pub fn seek_relative(&mut self, secs: f64) -> Result<(), Box<dyn Error>> {
        self.mpv.seek_forward(secs)?;
        Ok(())
    }
    pub fn toggle_playback(&mut self) -> Result<(), Box<dyn Error>> {
        if self.mpv.get_property("pause")? {
            self.unpause()?;
        } else {
            self.pause()?;
        }
        Ok(())
    }
    pub fn unpause(&mut self) -> Result<(), Box<dyn Error>> {
        self.mpv.unpause()?;
        Ok(())
    }
}

impl TryFrom<&str> for BrstmPlayer {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let brstm = BrstmFile::try_from(value)?;
        let mpv = Mpv::new();
        match mpv {
            Err(e) => Err(format!("ERROR: could not start mpv ({e})")),
            Ok(mpv) => Ok(BrstmPlayer {
                brstm,
                path: value.to_string(),
                mpv,
            }),
        }
    }
}

impl TryFrom<String> for BrstmPlayer {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let brstm = BrstmFile::try_from(value.as_str())?;
        let mpv = Mpv::new();
        match mpv {
            Err(e) => Err(format!("ERROR: could not start mpv ({e})")),
            Ok(mpv) => Ok(BrstmPlayer {
                brstm,
                path: value.to_string(),
                mpv,
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::BrstmPlayer;
    use std::{error::Error, thread, time::Duration};

    const TEST_FILE: &str = "./assets/DontDoIt.brstm";

    #[test]
    fn test_mpv_setup() -> Result<(), Box<dyn Error>> {
        let brstm = BrstmPlayer::try_from(TEST_FILE)?;
        brstm.setup()?;
        println!("{}", brstm.duration()?);

        thread::sleep(Duration::from_secs(35));

        Ok(())
    }
}
