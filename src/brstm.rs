use std::{
    error::Error,
    fs::File,
    io::{self, Read, Seek, SeekFrom},
};

enum_from_primitive! {
    #[derive(Debug, PartialEq)]
    pub enum HeaderOffsets {
        HEAD = 0x40,
        StreamDataInfoRef = 0x48,
    }
}

pub struct HeadContents {
    byte_order_mask: bool,
    tracks: u8,
    channels: u8,
    sample_rate: u32,
    loop_flag: bool,
    loop_start_sample: u32,
    end_sample: u32,
}

#[derive(Debug)]
pub struct BrstmFile {
    file: File,
    byte_order_mask: bool,
    tracks: u8,
    channels: u8,
    sample_rate: u32,
    loop_flag: bool,
    loop_start_sample: u32,
    end_sample: u32,
}

impl BrstmFile {
    pub fn file(&self) -> &File {
        &self.file
    }

    pub fn loop_start_sample(&self) -> u32 {
        self.loop_start_sample
    }

    pub fn loop_start_time(&self) -> f64 {
        self.loop_start_sample as f64 / self.sample_rate as f64
    }

    pub fn end_sample(&self) -> u32 {
        self.end_sample
    }

    pub fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    pub fn tracks(&self) -> u8 {
        self.tracks
    }

    pub fn loop_flag(&self) -> bool {
        self.loop_flag
    }

    pub fn channels(&self) -> u8 {
        self.channels
    }

    pub fn duration(&self) -> f64 {
        self.end_sample as f64 / self.sample_rate as f64
    }

    // pub fn set_loop_start(&mut self, sample: u32) {todo!()}

    pub fn open() {
        todo!()
    }

    pub fn new(file: File) -> Self {
        BrstmFile {
            file,
            byte_order_mask: false,
            tracks: 0,
            channels: 0,
            sample_rate: 0,
            loop_flag: false,
            loop_start_sample: 0,
            end_sample: 0,
        }
    }

    pub fn set_offset(&mut self, offset: u64) -> Result<(), io::Error> {
        self.file.seek(SeekFrom::Start(offset))?;
        Ok(())
    }

    pub fn skip(&mut self, offset: i64) -> Result<(), io::Error> {
        self.file.seek(SeekFrom::Current(offset))?;
        Ok(())
    }

    pub fn read_byte_at(&mut self, offset: u64) -> Result<u8, io::Error> {
        self.set_offset(offset)?;
        self.read_byte()
    }

    pub fn read_n_bytes_at(&mut self, n: usize, offset: u64) -> Result<Vec<u8>, io::Error> {
        self.set_offset(offset)?;
        self.read_n_bytes(n)
    }

    pub fn read_byte(&mut self) -> Result<u8, io::Error> {
        let byte = self.read_n_bytes(1)?;
        let byte = byte[0];
        Ok(byte)
    }

    pub fn read_n_bytes(&mut self, n: usize) -> Result<Vec<u8>, io::Error> {
        let mut bytes: Vec<u8> = vec![0; n];
        self.file.read_exact(&mut bytes[0..n])?;
        Ok(bytes)
    }

    pub fn read_u32(&mut self) -> Result<u32, io::Error> {
        let mut bytes: Vec<u8> = self.read_n_bytes(4)?;

        if self.byte_order_mask {
            bytes.reverse();
        }

        // Fails only if read_n_bytes returns something of length different than 4
        Ok(u32::from_be_bytes(bytes[0..].try_into().unwrap()))
    }

    pub fn read_u24(&mut self) -> Result<u32, io::Error> {
        let mut bytes: Vec<u8> = self.read_n_bytes(3)?;
        bytes.insert(0, 0);

        if self.byte_order_mask {
            bytes.reverse();
        }

        // Fails only if read_n_bytes returns something of length different than 3
        Ok(u32::from_be_bytes(bytes[0..].try_into().unwrap()))
    }

    pub fn read_u16(&mut self) -> Result<u16, io::Error> {
        let mut bytes: Vec<u8> = self.read_n_bytes(2)?;

        if self.byte_order_mask {
            bytes.reverse();
        }

        Ok(u16::from_be_bytes(bytes[0..].try_into().unwrap()))
    }

    pub fn read_u8(&mut self) -> Result<u8, io::Error> {
        self.read_byte()
    }

    /// Check the presence of the section's magic string
    fn check_magic_string(&mut self, magic: &str) -> Result<(), Box<dyn Error>> {
        let buf = self.read_n_bytes(4)?;
        let str = String::from(magic);
        let str = str.as_bytes();
        if buf.ne(str) {
            return Err(format!("Cannot read {magic} magic string"))?;
        }
        Ok(())
    }

    fn read_head(&mut self) -> Result<HeadContents, Box<dyn Error>> {
        // Check the magic string
        self.file.rewind()?;
        self.check_magic_string("RSTM")?;

        // Check the byte order mask
        let byte_order_mask = self
            .read_u8()
            .map_err(|e| format!("Cannot read byte_order_mask: {e}"))?;
        let byte_order_mask: bool = byte_order_mask == 0xFF;
        self.byte_order_mask = byte_order_mask;

        // The HEAD section is always adjacent to the header, which is always 0x40 in size
        self.set_offset(HeaderOffsets::HEAD as u64)?;
        self.check_magic_string("HEAD")?;

        // Skip the 01 00 00 00 byte sequence
        self.set_offset(HeaderOffsets::StreamDataInfoRef as u64 + 4)?;
        let stream_data_info: u32 = self
            .read_u32()
            .map_err(|e| format!("Cannot read stream_data_info: {e}"))?;
        // Go to the stream data info offset, considering the
        // previous offset of 4 to skip the byte sequence and the
        // just read 4 bytes
        self.skip(stream_data_info as i64 - 8)?;

        // Skipping as it is unused as of now (1 byte)
        // let format: u8 = self.read_u8()?;
        self.skip(1)?;
        let loop_flag: u8 = self
            .read_u8()
            .map_err(|e| format!("Cannot read loop_flag: {e}"))?;
        let loop_flag: bool = loop_flag != 0;
        let channels: u8 = self
            .read_u8()
            .map_err(|e| format!("Cannot read channels: {e}"))?;
        // Every track is stereo
        let tracks: u8 = channels / 2;
        let sample_rate: u32 = self
            .read_u24()
            .map_err(|e| format!("Cannot read sample_rate: {e}"))?;
        // Skip reserved bytes area (2 bytes)
        self.skip(2)?;
        let loop_start_sample: u32 = self
            .read_u32()
            .map_err(|e| format!("Cannot read loop_start_sample: {e}"))?;
        let end_sample: u32 = self
            .read_u32()
            .map_err(|e| format!("Cannot read end_sample: {e}"))?;

        Ok(HeadContents {
            byte_order_mask,
            tracks,
            channels,
            sample_rate,
            loop_flag,
            loop_start_sample,
            end_sample,
        })
    }

    pub fn print(&self) {
        println!("{:?}", self)
    }
}

impl TryFrom<File> for BrstmFile {
    type Error = String;
    fn try_from(value: File) -> Result<Self, Self::Error> {
        // Open the brstm from the specified file
        let mut brstm = BrstmFile::new(
            value
                .try_clone()
                .map_err(|_| "Cannot open file".to_string())?,
        );

        // Read the head of the brstm file
        let head_contents = brstm
            .read_head()
            .map_err(|e| format!("Cannot read brstm file: {e}"))?;

        // Return a brstm file with the read information
        Ok(BrstmFile {
            file: value,
            byte_order_mask: head_contents.byte_order_mask,
            tracks: head_contents.tracks,
            channels: head_contents.channels,
            sample_rate: head_contents.sample_rate,
            loop_flag: head_contents.loop_flag,
            loop_start_sample: head_contents.loop_start_sample,
            end_sample: head_contents.end_sample,
        })
    }
}

impl TryFrom<&str> for BrstmFile {
    type Error = String;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let file = File::open(value).map_err(|e| format!("Could not open file: {e}"))?;
        BrstmFile::try_from(file)
    }
}

#[cfg(test)]
mod tests {
    use super::BrstmFile;

    use super::*;

    const TEST_FILE: &str = "./assets/DontDoIt.brstm";

    #[test]
    fn test_open_file() {
        let file = File::open(TEST_FILE);
        assert!(file.is_ok());
    }

    #[test]
    fn test_read_byte() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let mut brstm = BrstmFile::new(file.unwrap());

        let result = brstm.read_byte_at(0)?;
        assert_eq!(result, 0x52);

        let result = brstm.read_byte_at(0x77)?;
        assert_eq!(result, 0x66);

        let result = brstm.read_byte_at(0x40)?;
        assert_eq!(result, 0x48);

        Ok(())
    }

    #[test]
    fn test_read_n_bytes() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let mut brstm = BrstmFile::new(file.unwrap());

        let result = brstm.read_n_bytes_at(4, 0)?;
        let expected = [0x52, 0x53, 0x54, 0x4D];
        assert_eq!(&result[..], expected);

        let result = brstm.read_n_bytes_at(4, 0x77)?;
        let expected = [0x66, 0x00, 0x00, 0x20];
        assert_eq!(&result[..], expected);

        let result = brstm.read_n_bytes_at(5, 0x112)?;
        let expected = [0xFD, 0x53, 0x0C, 0xC3, 0xFA];
        assert_eq!(&result[..], expected);

        Ok(())
    }

    #[test]
    fn test_read_u32() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let mut brstm = BrstmFile::new(file.unwrap());

        brstm.set_offset(0x68)?;
        let result = brstm.read_u32()?;
        let expected = 491950;
        assert_eq!(result, expected);

        brstm.set_offset(0x144)?;
        let result = brstm.read_u32()?;
        let expected = 832;
        assert_eq!(result, expected);

        Ok(())
    }

    #[test]
    fn test_read_u24() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let mut brstm = BrstmFile::new(file.unwrap());

        brstm.set_offset(0x63)?;
        let result = brstm.read_u24()?;
        let expected = 44100; // 0x00AC44
        assert_eq!(result, expected);

        Ok(())
    }

    #[test]
    fn test_read_u16() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let mut brstm = BrstmFile::new(file.unwrap());

        brstm.set_offset(0x04)?;
        let result = brstm.read_u16()?;
        let expected = 0xFEFF;
        assert_eq!(result, expected);

        brstm.set_offset(0x06)?;
        let result = brstm.read_u16()?;
        let expected = 0x0100;
        assert_eq!(result, expected);

        brstm.set_offset(0x0C)?;
        let result = brstm.read_u16()?;
        let expected = 0x40;
        assert_eq!(result, expected);

        Ok(())
    }

    #[test]
    fn test_read_head() -> Result<(), io::Error> {
        let file = File::open(TEST_FILE);
        let brstm = BrstmFile::try_from(file.unwrap()).unwrap();

        assert!(brstm.loop_flag());
        assert_eq!(brstm.tracks(), 1);
        assert_eq!(brstm.channels(), 2);
        assert_eq!(brstm.sample_rate(), 44100);
        assert_eq!(brstm.loop_start_sample(), 491950);
        assert_eq!(brstm.end_sample(), 1454321);

        brstm.print();

        Ok(())
    }
}
